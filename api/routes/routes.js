"use strict";
var controller = require('../controllers/controller.js')
module.exports = function(app){
    app.route('/config/deploy').post(
        controller.v1_deployRules
      );
}