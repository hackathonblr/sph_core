var definition = {
    "Product" : "prdCd",
    "Type" : "tp",
    "Reference-Id" : "refId",
    "Scheduled-On" : "schDtTm",
    "Amount" : "amt",
    "Currency" : 'ccy',
    "To.Financial Institution ID" : 'to[0].finInstId',
    "From.ID" : 'from.id',
    "To.ID" : 'to[0].id'
}

module.exports = definition;