var definition = {
    "_id": {
        "type": "String",
        "default": null
    },
    "prdCd": {
        "type": "String"
    },
    "tp": {
        "type": "String"
    },
    "refId": {
        "type": "String"
    },
    "schDtTm":{
        "type": "Number"
    },
    "from": {
        "id": {
            "type": "String"
        },
        "finInstId": {
            "type": "String"
        },
        "procUnitId": {
            "type": "String"
        }
    },
    "to": [
        {
            "id": {
                "type": "String"
            },
            "finInstId": {
                "type": "String"
            },
            "procUnitId": {
                "type": "String"
            }
        }
    ],
    "amt": {
        "type": "Number"
    },
    "ccy": {
        "type": "String"
    },
    "rmtInf": [
        {
            "type": "String"
        }
    ],
    "rmtKey": [
        {
            "type": "String"
        }
    ]
};
module.exports.definition=definition;