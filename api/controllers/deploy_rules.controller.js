"use strict";
const mongoose = require("mongoose");
const definition = require("../helpers/config.definition").definition;
const schema = new mongoose.Schema(definition);
const fileIO = require("../../lib/fileIO.js");
const pathValidator = "api/controllers/payment.validator.js";
const pathPreSave = "api/controllers/payment.controller.js";
var config = mongoose.model('Config',schema,'Config');  
var conditionKeys = require('../helpers/parser.definition.js');
var fileValidatorData = "";
var fileControllerData = "";

var deployRules = function(req, res) {
    fileValidatorData = fileIO.readFile(pathValidator + ".template");
    fileControllerData = fileIO.readFile(pathPreSave + ".template");
    config.find({product:req.body.product},{},{
    sort:{
        seq: 1
    }
    },function(err,data){
        if(err){res.send(err)}
        deployValidations(data);
        res.json({
            "message": "Deployed " + data.length + " rules",
            "rules": data
        });
    })
}

module.exports.deploy = deployRules;

function deployValidations(rules){
    rules.forEach(rule => {
        generateValidator(rule);
        setPreSave(rule);
    });
}

function generateValidator(rule){
    var defaultFunction = `
// THIS FUNCTION IS CODE GENERATED AUTOMATICALLY
validation._fnName = function(next){
    if(_condition){
        next(new Error("_message"))
    }else{
        next();
    }
}
`;
    console.log(rule.condition);
    rule.name = rule.name.toLowerCase();
    rule.name = rule.name.replaceAll(" " , "_");
    rule.name = rule.name.replaceAll(">" , "_gt_");
    rule.name = rule.name.replaceAll("<" , "_lt_");
    rule.name = rule.name.replaceAll("@" , "_at_");
    defaultFunction = defaultFunction.replaceAll('_fnName',rule.name);
    Object.keys(conditionKeys).forEach(key => {
        rule.condition = rule.condition.replaceAll(key,'this.'+conditionKeys[key]);
    });
    defaultFunction = defaultFunction.replaceAll('_condition',rule.condition);
    defaultFunction = defaultFunction.replaceAll('_message',rule.message);
    let index = fileValidatorData.indexOf("// #VALIDATION FUNCTIONS CODE-GEN END");
    fileValidatorData = fileValidatorData.slice(0,index)+defaultFunction+fileValidatorData.slice(index);
    fileIO.writeFile(pathValidator,fileValidatorData);

}

function setPreSave(rule){
    
    let preDefn = `schema.pre("save", validation._fnName); 
`;
preDefn = preDefn.replaceAll('_fnName',rule.name);
let index = fileControllerData.indexOf("// #PRE-SAVE CODE-GEN END");
fileControllerData = fileControllerData.slice(0,index)+preDefn+fileControllerData.slice(index);
fileIO.writeFile(pathPreSave,fileControllerData);

}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};