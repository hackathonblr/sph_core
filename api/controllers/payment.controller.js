"use strict";

const mongoose = require("mongoose");
const definition = require("../helpers/payment.definition.js").definition;
const SMCrud = require("swagger-mongoose-crud");
const cuti = require("cuti");
const schema = new mongoose.Schema(definition);
const logger = global.logger;
const validation = require('./payment.validator');

var options = {
    logger:logger,
    collectionName:"Payment"
};
schema.pre("save", cuti.counter.getIdGenerator("FT", "payment"));

// #PRE-SAVE CODE-GEN STARTS
schema.pre("save", validation.upi_from_vpa_must_have__at__symbol); 
schema.pre("save", validation.upi_to_vpa_must_have__at__symbol); 
schema.pre("save", validation.upi_transfer_limit__lt__100000); 
schema.pre("save", validation.upi_blacklist_test_bank); 
// #PRE-SAVE CODE-GEN END

var crudder = new SMCrud(schema,"Payment", options);
module.exports = {
    create:crudder.create,
    index:crudder.index,
    show:crudder.show,
    destroy:crudder.markAsDeleted,
    update:crudder.update
};
    