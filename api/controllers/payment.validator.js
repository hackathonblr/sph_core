var validation = {};

// #VALIDATION FUNCTIONS CODE-GEN STARTS


// THIS FUNCTION IS CODE GENERATED AUTOMATICALLY
validation.upi_from_vpa_must_have__at__symbol = function(next){
    if(!Contains(this.from.id , '@')){
        next(new Error("From VPA must contain '@'"))
    }else{
        next();
    }
}

// THIS FUNCTION IS CODE GENERATED AUTOMATICALLY
validation.upi_to_vpa_must_have__at__symbol = function(next){
    if(!Contains(this.to[0].id , '@')){
        next(new Error("To VPA must contain '@'"))
    }else{
        next();
    }
}

// THIS FUNCTION IS CODE GENERATED AUTOMATICALLY
validation.upi_transfer_limit__lt__100000 = function(next){
    if(this.amt < 0 || this.amt >= 100000){
        next(new Error("Transaction amount is not valid for this service."))
    }else{
        next();
    }
}

// THIS FUNCTION IS CODE GENERATED AUTOMATICALLY
validation.upi_blacklist_test_bank = function(next){
    if(Uppercase(this.to[0].finInstId) == 'TESTBANK'){
        next(new Error("Cannot make payments to financial institution TESTBANK"))
    }else{
        next();
    }
}
// #VALIDATION FUNCTIONS CODE-GEN END

module.exports = validation;

//Utility Functions
function Contains(findIn,val){
    return findIn.indexOf(val) != -1 ? true : false;
}
function GetDayOfWeek(date){

}
function GetDayOfMonth(date){

} 
function GetHourOfDay(date){

}
function GetMinuteOfDay(date){

}
function Now(){}
function Lowercase(text){ return text.toLowerCase()}
function Uppercase(text){ return text.toUpperCase()}
