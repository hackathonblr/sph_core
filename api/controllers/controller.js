"use strict";
//controllers
const paymentController = require("./payment.controller.js");
const deployRulesController = require("./deploy_rules.controller.js");
//exports
var exports = {};
exports.v1_paymentCreate = paymentController.create;
exports.v1_paymentList = paymentController.index;
exports.v1_paymentShow = paymentController.show;
exports.v1_paymentDestroy = paymentController.destroy;
exports.v1_paymentUpdate = paymentController.update;
exports.v1_deployRules = deployRulesController.deploy;

module.exports = exports;
    