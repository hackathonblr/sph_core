const fs = require("fs");

function readFile(name,notJson){
    return fs.readFileSync(name, 'utf8');
}


function writeFile(path, data){
    return new Promise((resolve,reject) => {
        fs.writeFile(path, data, { flag: "w" }, err => {
            if(err)
                reject(err);
            else
                resolve();
        });
    });
}

function checkIffileExists(fileName){
    try{
        fs.statSync(fileName);
        return true;
    }
    catch(err){
        return false;
    }
}



module.exports.checkIffileExists = checkIffileExists;
module.exports.writeFile = writeFile;
module.exports.readFile = readFile;